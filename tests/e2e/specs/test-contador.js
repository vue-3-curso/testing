describe('Counter features',()=>{
    //Comprobar si la página es accesible
    it('User can access to page',()=>{
        cy.visit('/about')
        //el contenido que se muestra es correcto.
        cy.contains('h1','Counter App')
    }),
    //Comprobar si el contador si incrementa
    it('user can increment counter',()=>{
        //comprobar que se visita la url
        cy.visit('/about')
        //obtenemos el elemento y comprobamos que el texto que debería mostrar es correcto
        cy.get('#counter-info').contains('Contador 0')
        //comprobar que se encuentra el botón de incrementar el contador y hacemos click
        cy.contains('button','Aumentar').click()
        //comprobamos que el valor ha aumentado
        cy.get('#counter-info').contains('Contador 1')
    })
})